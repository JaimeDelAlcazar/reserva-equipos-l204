import React, {useRef, useState} from 'react'
import { Form, Col, Row, Button, Table, Alert } from 'react-bootstrap'
import Link from 'next/link'
import Fetch from 'isomorphic-unfetch'
import moment from 'moment'


import PanelSeleccion from '../PanelSeleccion/PanelSeleccion';
import config from '../../config.js';

import Email from '../../smtp.js';


import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";

// import '../../date_picker.css'


const PanelSolicitud = ({ props }) => {
    console.log('Props',props)

    const nombre_proyecto  =   useRef();
    const ubicacion  =   useRef();
    const nombre_solicitante  =   useRef();
    const correo_solicitante  =   useRef();
    const coord_campo  =   useRef();
    const correo_coord_campo  =   useRef();
    const coord_tecnico  =   useRef();
    const correo_coord_tecnico  =   useRef();
    const coord_logistico  =   useRef();
    const correo_coord_logistico  =   useRef();
    const fecha_salida  =   useRef();
    const fechar_retorno  =   useRef();


    // 
    const buscador  =   useRef(null);
    // const buscador  =   useRef();


    const [changed, setChanged] = useState([]);

    const table_header = ['Nombre','Modelo','Especialista','Inicio mant. predictivo','Fin mant. predictivo','Fecha salida','Fecha retorno','Fin mant. correctivo','Solicitar'];

    const [showMe, setShowMe] = useState(false)
    const [json, setJson] = useState('')

    const [fSalida, setFSalida] = useState('')
    const [fRetorno, setFRetorno] = useState('')



    const filtrar = () => {
        // console.log(buscador.current?.value)

        // if (typeof buscador.current?.value !== 'undefined') {

            const texto = buscador.current?.value.toLowerCase();

            for (let i = 0; i < props.length; i++) {
    
                if (((props[i].nombre).toLowerCase().indexOf(texto) !== -1) || ((props[i].modelo)?.toLowerCase().indexOf(texto) !== -1)) {
                    document.getElementsByName('row_equipo')[i].style.display = "table-row";
                } else {
                    document.getElementsByName('row_equipo')[i].style.display = "none";
                }
            }

        // }
        
        
    }



    const solicitar = () => {
        // alert(getSelectedCheckbox('selected') )
        // displayCheckedCheckbox();
        alertFields();
        // alertMail();
    }    




    async function postDataEquipo() {

        document.getElementsByClassName('btn_confirmar_solicitud')[0].disabled = true;

        console.log('changed = ',changed);
        const allSelect = document.querySelectorAll('select');

        const pos = getSelectedCheckbox('selected');
        let unavailable = []
        unavailable = verifyDisponibility(pos);
        console.log('unavailable equipments',unavailable)
        if (unavailable.length >0) {
            console.log('exit')
            document.getElementsByClassName('btn_confirmar_solicitud')[0].disabled = false;
            return
        }

        var data = [];
        var json_equipo ={};

        var current_time = moment().format();   


        // 

        document.getElementsByClassName('processMessage')[0].style.display = "block";

        setTimeout(function() {
            document.getElementsByClassName('processMessage')[0].style.display = "none";
            
          }, 3000);

        // 

        let selectedEquipos = [];

        await pos.forEach( async (x) => {
            // 
            json_equipo =  {
                nombre_proyecto: nombre_proyecto.current?.value,
                ubicacion: ubicacion.current?.value,
                nombre_solicitante: nombre_solicitante.current?.value,
                correo_solicitante: correo_solicitante.current?.value,
                coord_campo: coord_campo.current?.value,
                correo_coord_campo: correo_coord_campo.current?.value,
                coord_tecnico: coord_tecnico.current?.value,
                correo_coord_tecnico: correo_coord_tecnico.current?.value,
                coord_logistico: coord_logistico.current?.value,
                correo_coord_logistico: correo_coord_logistico.current?.value,
                estado_solicitud: 'Pendiente', //Pendiente,Aprobado,Rechazado,Finalizado
              //   equipo_id: equipo,
                equipo_id: props[x].id,
                nombre_equipo: props[x].nombre,
                modelo_eq: props[x].modelo,
                marca_eq: props[x].marca,
                nro_serie_eq: props[x].nro_serie,
                nro_inv_eq: props[x].nro_inventario,
                // especialista: props[x].especialista_1,
                especialista: allSelect[x].value? allSelect[x].value:'',
                // fecha_salida_progr: fecha_salida.current?.value,
                fecha_salida_progr: moment(startDateSalida).format("DD/MM/YYYY"),
                // fecha_devolucion_progr: fechar_retorno.current?.value,
                fecha_devolucion_progr: moment(startDateRetorno).format("DD/MM/YYYY"),
                fecha_ini_mant_predictvo: moment(props[x].fecha_inicio_mant_predictivo).format('DD/MM/YYYY '),
                fecha_fin_mant_predictivo: moment(props[x].fecha_fin_mant_predictivo).format('DD/MM/YYYY '),
              //   fecha_fin_mant_correctivo: moment(db_fecha_fin_mant_corr).format('DD/MM/YYYY    '),
                fecha_fin_mant_correctivo: moment(moment(props[x].fecha_devolucion).add(props[x].periodo_mant_correctivo,'days').format()).format('DD/MM/YYYY '),

                // fecha_registro: current_time,
                fecha_registro: moment(current_time).format("DD/MM/YYYY"),
              }




            data.push(json_equipo);

            selectedEquipos.push(props[x].nombre)

        })
        // console.log('data',JSON.stringify(data));
        console.log('data',data);

        const response = await fetch(`${config.domain}/api/solicitud/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })

        // let list = [];
        let eq_str = '<ul>'
        for (let i = 0; i < selectedEquipos.length; i++) {
            eq_str = eq_str + '<li>' + selectedEquipos[i] + '</li>'
        }
        eq_str = eq_str + '<ul>'


        // 

        

        // var Email;
        // <script src="https://smtpjs.com/v3/smtp.js"></script>
        await Email.send({
        // await window.Email.send({
            Host : "smtp.gmail.com",
            // Username : "jdelalcazar@utec.edu.pe",
            Username : "lmarin@utec.edu.pe",
            // Password : "Ut3c@24J77",
            // Password : "marinlynnguadalupe@10A",
            Password : "@10Alynn",
            To : correo_solicitante.current?.value,
            // From : "jdelalcazar@utec.edu.pe",
            From : "lmarin@utec.edu.pe",
            Subject : "Registro de Solicitud de Reserva",
            Body : "La solicitud de reserva de equipos ha sido registrada. Se le enviará un correo de confirmación cuando la solicitud sea procesada."  + '<br/>' + '<br/>' + '<div>' + "Equipos solicitados: " + eq_str + '</div>' + '<br/>' + "CITA, UTEC.",

        }).then(
        console.log('enviado')
        );

        // 
        await Email.send({
            // await window.Email.send({
                Host : "smtp.gmail.com",
                // Username : "jdelalcazar@utec.edu.pe",
                Username : "lmarin@utec.edu.pe",
                // Password : "Ut3c@24J77",
                // Password : "marinlynnguadalupe@10A",
                Password : "@10Alynn",
                To : "lmarin@utec.edu.pe",
                // From : "jdelalcazar@utec.edu.pe",
                From : "lmarin@utec.edu.pe",
                Subject : "Registro de Solicitud de Reserva",
                Body : "La solicitud de reserva de equipos ha sido registrada. Se le enviará un correo de confirmación cuando la solicitud sea procesada."  + '<br/>' + '<br/>' + '<div>' + "Equipos solicitados: " + eq_str + '</div>' + '<br/>' + "CITA, UTEC.",

            }).then(
            console.log('enviado')
            );
    
        await showSuccess();

    }




    function alertFields() {
        // const fields = document.querySelectorAll(`input[name="field"]`);
        const fields = document.querySelectorAll(`input[name="field"], input[name="dateField"]`);

        // const dateFields = document.querySelectorAll(`input[name="dateField"]`);

        // fields.push(dateFields);
        // console.log('fields input',fields)

        for (let i = 0; i < fields.length; i++) {
            // console.log('fields[i].value',fields[i].value);
            if (fields[i].value.length > 0) {
                
            } else {

                document.getElementsByClassName('alert_allFields')[0].style.display = "block";

                return
            }
            
        };

        
        document.getElementsByClassName('alert_allFields')[0].style.display = "none";


        // console.log('fecha_salida',fecha_salida.value)
        console.log('fecha_salida',startDateSalida)
        // console.log('fechar_retorno',fechar_retorno.current?.value)
        console.log('fechar_retorno',startDateRetorno)


;
        // const f_salida_input = startDateSalida;
        const f_salida_input = moment(startDateSalida).format();  
        // const f_retorno_input = moment(startDateRetorno, "DDMMYYYY").utc().format();

        // const f_retorno_input = startDateRetorno;
        const f_retorno_input = moment(startDateRetorno).format();  

        console.log('f_salida_input',f_salida_input)
        console.log('f_retorno_input',f_retorno_input)

        // const f_today = moment().utc().format("DDMMYYYY");  
        const f_today = moment().format();  

        console.log('f_today',f_today)

        if ((f_retorno_input >= f_salida_input)&&(f_salida_input >= f_today)) {
            console.log('OK');
        } else {
            console.log('Fechas inválidas');

            document.getElementsByClassName('alert_invalidDates')[0].style.display = "block";

            return
        }
        document.getElementsByClassName('alert_invalidDates')[0].style.display = "none";



        // 
        // alertMail();
        if ((correo_solicitante.current?.value.indexOf('@utec.edu.pe') == -1) || (correo_coord_campo.current?.value.indexOf('@utec.edu.pe') == -1) || (correo_coord_tecnico.current?.value.indexOf('@utec.edu.pe') == -1) || (correo_coord_logistico.current?.value.indexOf('@utec.edu.pe') == -1)) {
            // alert('Los correos deben pertenecer a UTEC.')
            // alert_invalidMail
            document.getElementsByClassName('alert_invalidMail')[0].style.display = "block";
            return
        } else {
            document.getElementsByClassName('alert_invalidMail')[0].style.display = "none";
        }

        // 
        // setFSalida(fecha_salida.current?.value);
        // setFSalida(startDateSalida);
        // setFRetorno(fechar_retorno.current?.value);
        // setFRetorno(startDateRetorno);
        setFSalida(moment(startDateSalida).format("DD/MM/YYYY"));
        setFRetorno(moment(startDateRetorno).format("DD/MM/YYYY"));


        document.getElementsByClassName('panel_solicitar')[0].style.display = "flex";
        document.getElementsByClassName('panel_formulario')[0].style.display = "none";
    }

    function getSelectedCheckbox(selected) {
        const checkboxes = document.querySelectorAll(`input[name="${selected}"]`);
        // const checkboxes = document.querySelectorAll(`input[name="${selected}"]:checked`);
        let values = [];
        /* checkboxes.forEach((checkbox) => {
            values.push(checkbox.value);
        }); */

        for (let i = 0; i < checkboxes.length; i++) {
            // const element = checkboxes[i];
            if (checkboxes[i].checked ==  true) {
                // values.push(props[i]['id'])
                values.push(i)
            }
        }


        return values;
    }

    function displayCheckedCheckbox() {
        // let only_checked = false;
        var checkBoxDisplay = document.getElementById("display_selected");
        const checkboxes = document.querySelectorAll(`input[name="selected"]`);
        // let values = [];

        
            for (let i = 0; i < checkboxes.length; i++) {
                // const element = checkboxes[i];
                

                if (checkboxes[i].checked ==  false) {
                    if (checkBoxDisplay.checked == true) {
                        document.getElementsByName('row_equipo')[i].style.display = "none";
                    } else {
                        document.getElementsByName('row_equipo')[i].style.display = "table-row";
                    }
                }
                
            }
        
        // only_checked = true;

    }

    function showAlertNotEquipment(showAlert,ids) {
        if (showAlert == true) {
            document.getElementsByClassName('alert_not_equipo')[0].style.display = "block";
            if (ids.length == 1) {
                document.getElementsByClassName('alert_not_equipo')[0].innerHTML = "El equipo "+`${ids[0] +1}`+" no se encuentra disponible."
            }
            if (ids.length > 1) {
                let ids_string = "";
                for (let i = 0; i < ids.length; i++) {
                    // const element = ids[i];
                    // let ids_string = ""+ids[i]+""
                    ids_string = ""+ ids_string +" "+ `${ids[i] +1}` +","
                }

                document.getElementsByClassName('alert_not_equipo')[0].innerHTML = "Los equipos "+ids_string+" no se encuentran disponibles."
                // document.getElementsByClassName('alert_not_equipo')[0].innerHTML = "Los equipos "+ids[0]+" no se encuentran disponibles."
            }
            
        } else {
            document.getElementsByClassName('alert_not_equipo')[0].style.display = "none";
        }
        
    }

    function showSuccess() {
        document.getElementsByClassName('successMessage')[0].style.display = "block";
        // document.getElementsByClassName('btn_confirmar_solicitud')[0].disabled = true;
        setTimeout(function() {
            document.getElementsByClassName('successMessage')[0].style.display = "none";

            //your code to be executed after 1 second
            document.getElementsByClassName('btn_confirmar_solicitud')[0].disabled = false;
            
            // Redirect to home
            window.location.href = "/";
        //   }, 3500);
          }, 3000);
    }




    function verifyDisponibility(ids) {
        // const f_salida_input = moment(fecha_salida.current?.value, "DDMMYYYY").utc().format();
        const f_salida_input = moment(startDateSalida).format(); 
        // const f_retorno_input = moment(fechar_retorno.current?.value, "DDMMYYYY").utc().format();
        const f_retorno_input = moment(startDateRetorno).format(); 

        console.log('verifyDisponibility f_salida_input',f_salida_input)
        console.log('verifyDisponibility f_retorno_input',f_retorno_input)

        console.log('ids',ids);

        let unavailable = [];

        for (let i = 0; i < ids.length; i++) {
            // const element = array[i];
            let equipo_index = ids[i];
            console.log('equipo_index',equipo_index);

            const db_fecha_salida = moment(props[equipo_index].fecha_salida).format();

            const db_fecha_fin_mant_corr = moment(props[equipo_index].fecha_devolucion).add(props[equipo_index].periodo_mant_correctivo,'days').format();

            
            const db_fecha_ini_mant_pred = props[equipo_index].fecha_inicio_mant_predictivo;
            const db_fecha_fin_mant_pred = props[equipo_index].fecha_fin_mant_predictivo;

            // console.log('-------------------------------');
            // console.log('f_salida_input',f_salida_input);
            // console.log('db_fecha_salida',db_fecha_salida);
            // console.log('db_fecha_fin_mant_corr',db_fecha_fin_mant_corr);
            // console.log('f_retorno_input',f_retorno_input);


            // Comparacion con fechas salida y retorno
            if ((props[equipo_index].fecha_salida)&&(props[equipo_index].fecha_devolucion)) {

                console.log('db_fecha_fin_mant_corr',db_fecha_fin_mant_corr)

                if ((((f_salida_input >= db_fecha_salida)&&(f_salida_input <= db_fecha_fin_mant_corr)) || ((f_retorno_input >=  db_fecha_salida)&&(f_retorno_input <= db_fecha_fin_mant_corr)))) {
                    // console.log('no disponible')
                    unavailable.push(equipo_index);
                    // showAlertNotEquipment(true);
                    
                    // return
                } else {
                    console.log('disponible')
                    // showAlertNotEquipment(false);
                }
            }
            // Comparacion con fechas de mantenimiento predictivo
            if ((props[equipo_index].fecha_salida)&&(props[equipo_index].fecha_devolucion)&&(props[equipo_index].fecha_inicio_mant_predictivo)&&(props[equipo_index].fecha_fin_mant_predictivo)) {
                if ((((f_salida_input >= db_fecha_ini_mant_pred)&&(f_salida_input <= db_fecha_fin_mant_pred)) || ((f_retorno_input >=  db_fecha_ini_mant_pred)&&(f_retorno_input <= db_fecha_fin_mant_pred)))) {
                    // console.log('no disponible')
                    unavailable.push(equipo_index);
                    // showAlertNotEquipment(true);
                    
                    // return
                } else {
                    // console.log('disponible')
                    // showAlertNotEquipment(false);
                }
            }
                
            }

            if (unavailable.length == 0) {
                showAlertNotEquipment(false);
            } else {
                showAlertNotEquipment(true,unavailable);
                // showAlertNotEquipment(true,[1,4,6]);
            }

            return unavailable;

    }

   


    // const [startDate, setStartDate] = useState(new Date());
    const [startDateSalida, setStartDateSalida] = useState(null);

    const [startDateRetorno, setStartDateRetorno] = useState(null);


    return (
        <>
        <script src="https://smtpjs.com/v3/smtp.js"></script> 
            <div className="contenedor_principal panel_formulario">

            <p className="titulo_panel_principal">Solicitud de Reserva</p>

            <Form className="form_principal">
            
            <Form.Group>
                    {/* <Alert key={idx} variant={variant}> */}
            <Alert variant='danger' className="alert_invalidMail">
                Los correos deben pertenecer a UTEC.
            </Alert>
            <Alert variant='danger' className="alert_invalidDates">
                Las fechas son inválidas.
            </Alert>
            <Alert variant='danger' className="alert_allFields">
                {/* This is a {variant} alert—check it out! */}
                Por favor, llenar todos los campos.
            </Alert>
            
            <Row className="row_input">
                <Col>
                <Form.Control size="lg" type="text" placeholder="Nombre del Proyecto"  className="input_text" ref={nombre_proyecto} name="field"/>
                </Col>
                <Col>
                <Form.Control size="lg" type="text" placeholder="Ubicación" className="input_text" ref={ubicacion} name="field"/>
                </Col>
            </Row>
            </Form.Group>
            <Form.Group>
            <Row className="row_input">
                <Col>
                <DatePicker dateFormat="dd/MM/yyyy" className="input_date" selected={startDateSalida} placeholderText="Fecha de Salida (DD/MM/YYYY)" onChange={(date) => setStartDateSalida(date)}  ref={fecha_salida} name="dateField"/>
                </Col>
                <Col>
                <DatePicker dateFormat="dd/MM/yyyy" className="input_date" selected={startDateRetorno} placeholderText="Fecha de Retorno (DD/MM/YYYY)" onChange={(date) => setStartDateRetorno(date)}  ref={fechar_retorno} name="dateField"/>
                </Col>
            </Row>
            </Form.Group>
            <Form.Group>
            <Row className="row_input">
                <Col>
                <Form.Control  size="lg" placeholder="Nombre del Solicitante" className="input_text" ref={nombre_solicitante} name="field"/>
                </Col>
                <Col>
                <Form.Control size="lg"  placeholder="Correo del Solicitante" className="input_text" ref={correo_solicitante} name="field"/>
                </Col>
            </Row>
            </Form.Group>
            <Form.Group>
            <Row className="row_input">
                <Col>
                <Form.Control  size="lg" placeholder="Coordinador de Campo" className="input_text" ref={coord_campo} name="field"/>
                </Col>
                <Col>
                <Form.Control size="lg"  placeholder="Correo del Coordinador de Campo" className="input_text" ref={correo_coord_campo} name="field"/>
                </Col>
            </Row>
            </Form.Group>
            <Form.Group>
            <Row className="row_input">
                <Col>
                <Form.Control  size="lg" placeholder="Coordinador Técnico" className="input_text" ref={coord_tecnico} name="field"/>
                </Col>
                <Col>
                <Form.Control size="lg"  placeholder="Correo del Coordinador Técnico" className="input_text" ref={correo_coord_tecnico} name="field"/>
                </Col>
            </Row>
            </Form.Group>
            <Form.Group>
            <Row className="row_input">
                <Col>
                <Form.Control  size="lg" placeholder="Coordinador Logístico" className="input_text" ref={coord_logistico} name="field"/>
                </Col>
                <Col>
                <Form.Control size="lg"  placeholder="Correo del Coordinador Logístico" className="input_text" ref={correo_coord_logistico} name="field"/>
                </Col>
            </Row>
            </Form.Group>
            </Form>

            <Button variant="info" size="lg" className="btn_solicitar_equipos" onClick={solicitar}>Solicitar equipos</Button>
        </div>


            <div className="contenedor_seleccion panel_solicitar">

                <Alert variant='success' className="processMessage" >Se está procesando la solicitud, esto podría tomar unos minutos.</Alert>


                <Alert variant='success' className="successMessage" >Se registró la solicitud, se le enviará un correo de confirmación.</Alert>

                <Form className="form_seleccion">
                <Form.Row className="form_row_search">
                    <Form.Group className="search_container">
                    <Form.Control type="text" placeholder="Buscar equipos ..."    className="input_search" ref={buscador} onKeyUp={filtrar}/>
                    </Form.Group>

                    <span style={{display: 'flex', flexDirection: 'column',justifyContent: 'center', alignItems: 'center', marginTop: '-10px'}}><div>Fecha de reserva</div><div>{fSalida} - {fRetorno}</div></span>

                    <span className="checkbox_label">Mostrar seleccionados <input id="display_selected" type="checkbox"  onClick={displayCheckedCheckbox}/></span>

                </Form.Row>

                {/* <Alert variant='danger' className="alert_allFields"> */}
                <Alert variant='success' className="alert_success_equipo" >
                {/* This is a {variant} alert—check it out! */}
                El equipo se encuentra disponible.
                </Alert>

                <Alert variant='danger' className="alert_not_equipo" >
                {/* This is a {variant} alert—check it out! */}
                El equipo no se encuentra disponible.
                </Alert>

                <Table responsive >
                <thead>
                    <tr>
                    <th className="th_simple">id</th>
                    { Array.from({ length: table_header.length }).map((_, index) => (
                        <th key={index} className="th_table_equipos">{table_header[index]}</th>
                    ))}
                    </tr>
                </thead>
                <tbody>

                    {
                        props.map(function(equipo, idx){
                            return (
                                <tr key={idx} name="row_equipo">

                                    <td className="td_vertical_middle">{equipo.id}</td>
                                    <td className="td_vertical_middle">{equipo.nombre}</td>

                                    <td className="td_vertical_middle">{equipo.modelo}</td>

                                    <td className="td_vertical_middle">

                                    <select defaultValue={equipo.especialista_1 == null? (equipo.especialista_2 == null? (equipo.especialista_3 == null? (equipo.especialista_4 == null? null : equipo.especialista_4) : equipo.especialista_3) : equipo.especialista_2) : equipo.especialista_1} onChange={e => (console.log('Value changed', e.target.value))} className="selectEspecialista">
                                    
                                        {typeof equipo.especialista_1 !== 'undefined' ? (equipo.especialista_1 !== '' ? <option>{equipo.especialista_1}</option> : null )  :null}
                                        {typeof equipo.especialista_2 !== 'undefined' ? (equipo.especialista_2 !== '' ? <option>{equipo.especialista_2}</option> : null )  :null}
                                        {typeof equipo.especialista_3 !== 'undefined' ? (equipo.especialista_3 !== '' ? <option>{equipo.especialista_3}</option> : null )  :null}
                                        {typeof equipo.especialista_4 !== 'undefined' ? (equipo.especialista_4 !== '' ? <option>{equipo.especialista_4}</option> : null )  :null}

                                    </select>
                                    </td>
                                    {/* <td>{equipo.fecha_inicio_mant_predictivo}</td> */}
                                    <td className="td_table_center">{equipo.fecha_inicio_mant_predictivo == null? null : moment(equipo.fecha_inicio_mant_predictivo).format('DD/MM/YYYY')}</td>
                                    {/* <td>{equipo.fecha_fin_mant_predictivo}</td> */}
                                    <td className="td_table_center">{equipo.fecha_fin_mant_predictivo == null? null : moment(equipo.fecha_fin_mant_predictivo).format('DD/MM/YYYY')}</td>
                                    {/* <td>{equipo.fecha_salida}</td> */}
                                    <td className="td_table_center">{equipo.fecha_salida == null? null : moment(equipo.fecha_salida).format('DD/MM/YYYY ')}</td>
                                    {/* <td>{equipo.fecha_devolucion}</td> */}
                                    <td className="td_table_center">{equipo.fecha_devolucion == null? null : moment(equipo.fecha_devolucion).format('DD/MM/YYYY ')}</td>
                                    {/* <td> </td> */}
                                    {/* <td className="td_table_center">{equipo.fecha_devolucion == null || equipo.periodo_mant_correctivo == null? null : moment(equipo.fecha_devolucion).add(equipo.periodo_mant_correctivo,'days').format('DD/MM/YYYY ')}</td> */}
                                    <td className="td_table_center">
                                        {
                                        equipo.fecha_devolucion == null? (null) : (equipo.periodo_mant_correctivo == null? moment(equipo.fecha_devolucion).add(0,'days').format('DD/MM/YYYY ') : moment(equipo.fecha_devolucion).add(equipo.periodo_mant_correctivo,'days').format('DD/MM/YYYY '))
                                        }</td>
                                    <td className="td_table_center"><input type="checkbox" name="selected"/>
                                    {/* <td className="td_table_center"><input type="checkbox" onClick="" /> */}

                                    </td>
                                </tr>
                            )
                        })
                    }        

                </tbody>
                </Table>

                <Button variant="info" size="lg"  className="btn_confirmar_solicitud" onClick={postDataEquipo}> Confirmar solicitud</Button>

                </Form>


            </div>

        </>
    )
}




export default PanelSolicitud
