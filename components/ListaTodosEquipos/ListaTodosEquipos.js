import React, {useRef, useState} from 'react'

import { Form, Container, Col, Row, Button, Table } from 'react-bootstrap'
import Link from 'next/link'
import moment from 'moment'


const ListaTodosEquipos = ({props}) => {

    // console.log('props',props);

    const buscador  =   useRef();

    const table_header = ['Nombre','Modelo','Marca','Seguro','Carrera / Proyecto','Número de serie','Número de inventario','Especialista','Inicio mant. preventivo','Fin mant. preventivo','Fecha próxima de salida','Fecha próxima de devolución','Ubicación','Periodo de mant. correctivo','Encargado de mant. preventivo'];
    // const table_header = ['Nombre','Modelo','Marca','Carrera / Proyecto','Número de serie','Número de inventario','Especialista','Inicio mant. preventivo','Fin mant. preventivo','Fecha próxima de salida','Fecha próxima de devolución','Ubicación','Periodo de mant. correctivo','Encargado de mant. preventivo'];
    // const table_header = ['Nombre','Modelo','Marca','Carrera / Proyecto','Número de serie','Número de inventario','Especialista','Inicio mant. predictivo','Fin mant. predictivo','Fecha próxima de salida','Fecha próxima de devolución','Ubicación','Periodo de mant. correctivo','Encargado de mant. predictivo'];
    // const table_header = ['Nombre','Modelo','Marca','Carrera / Proyecto','Número de serie','Número de inventario','Especialista','Fecha inicio de mantenimiento predictivo','Fecha fin de mantenimiento predictivo','Fecha próxima de salida a campo','Fecha próxima de devolución a UTEC','Ubicación','Periodo de mantenimiento correctivo','Encargado de mantenimiento predictivo'];
    // const table_header = ['Nombre','Modelo','Marca','Carrera / Proyecto','Número de serie','Número de inventario','Especialista','Fecha inicio de mantenimiento predictivo','Fecha fin de mantenimiento predictivo','Fecha próxima de salida a campo','Fecha próxima de devolución a UTEC','Ubicación','Periodo de mantenimiento correctivo'];
    // const table_header = ['Nombre','Modelo','Marca','Carrera / Proyecto','Número de serie','Número de inventario','Especialista','Estado','Fecha inicio de mantenimiento predictivo','Fecha fin de mantenimiento predictivo','Fecha de salida a campo','Fecha de devolución a UTEC','Ubicación','Periodo de mantenimiento correctivo'];



    const filtrar = () => {
        // console.log(buscador.current?.value)
        const texto = buscador.current?.value.toLowerCase();

        for (let i = 0; i < props.length; i++) {

            if (((props[i].nombre).toLowerCase().indexOf(texto) !== -1) || ((props[i].modelo).toLowerCase().indexOf(texto) !== -1) || ((props[i].marca).toLowerCase().indexOf(texto) !== -1)  || ((props[i].proyecto_carrera).toLowerCase().indexOf(texto) !== -1)  ) {

                (document.getElementsByClassName('row_equipo'))[i].style.display = "table-row";
                console.log('props[i]',props[i])
            } else {
                document.querySelectorAll('.row_equipo')[i].style.display = "none";
            }



        }

    }


    

    return (
        <div className="contenedor_seleccion">
            <div className="form_lista_equipos">
                {/* { <p>{moment().format()}</p> } */}


            <input type="text" placeholder="Buscar equipos ..." className="lista_search" defaultValue='' ref={buscador} onKeyUp={filtrar}/>


            <Table responsive bordered className="table">
            <thead>
                <tr>
                <th className="th_simple">id</th>
                {/* <th>#</th> */}
                {Array.from({ length: table_header.length }).map((_, index) => (
                    <th key={index} className="th_table_equipos">{table_header[index]}</th>
                ))}
                </tr>
            </thead>
            <tbody>

                {
                    props.map(function(equipo, idx){
                        // var especialista_ = equipo.especialista_1 + " " + equipo.especialista_2 + " " + equipo.especialista_3 + " " + equipo.especialista_4;
                        // var especialista_ = equipo.especialista_1 + "\n\n" + equipo.especialista_2 + "\n\n" + equipo.especialista_3 + "\n\n" + equipo.especialista_4;
                        var especialista_ = [];
                        especialista_.push(equipo.especialista_1,equipo.especialista_2,equipo.especialista_31,equipo.especialista_4);
                        // var especialista = especialista_.split(" ");
                        var especialista = especialista_.filter(function (val) {if (val != null) return val;}).join(', ');
                        return (
                            <tr key={idx} className="row_equipo">
                                {/* <td  className="td_table_center">{idx + 1}</td> */}
                                <td className="td_horizontal_center">{equipo.id}</td>
                                <td>{equipo.nombre}</td>
                                <td>{equipo.modelo}</td>
                                <td>{equipo.marca}</td>
                                <td>{equipo.seguro}</td>
                                <td>{equipo.proyecto_carrera}</td>
                                <td>{equipo.nro_serie}</td>
                                <td>{equipo.nro_inventario}</td>
                                {/* <td>{(equipo.especialista_1 + " " + equipo.especialista_2 + " " + equipo.especialista_3 + " " + equipo.especialista_4 ).split(" ")}</td> */}
                                {/* <td>{especialista_.split(" ")}</td> */}
                                <td>{especialista}</td>
                                {/* <td>{equipo.especialista_1} <br/> {equipo.especialista_2} <br/> {equipo.especialista_3} <br/> {equipo.especialista_4 }</td> */}
                                {/* <td>{equipo.estado}</td> */}
                                {/* <td>{equipo.fecha_inicio_mant_predictivo}</td> */}
                                <td>{equipo.fecha_inicio_mant_predictivo == null? null : moment(equipo.fecha_inicio_mant_predictivo).format('DD/MM/YYYY ')}</td>
                                {/* <td>{equipo.fecha_fin_mant_predictivo}</td> */}
                                <td>{equipo.fecha_fin_mant_predictivo == null? null : moment(equipo.fecha_fin_mant_predictivo).format('DD/MM/YYYY ')}</td>
                                {/* <td>{equipo.fecha_salida}</td> */}
                                {/* <td>{moment(equipo.fecha_salida).format('L')}</td> */}
                                {/* <td>{moment(equipo.fecha_salida).format('YYYY-MM-DD HH:mm:ss')}</td> */}
                                <td>{equipo.fecha_salida == null? null : moment(equipo.fecha_salida).format('DD/MM/YYYY ')}</td>
                                {/* <td>{equipo.fecha_devolucion}</td> */}
                                <td>{equipo.fecha_devolucion == null? null : moment(equipo.fecha_devolucion).format('DD/MM/YYYY ')}</td>
                                <td>{equipo.ubicacion}</td>
                                {/* <td className="td_horizontal_center">{equipo.periodo_mant_correctivo + " días"}</td> */}
                                <td className="td_horizontal_center">{equipo.periodo_mant_correctivo == null? null : equipo.periodo_mant_correctivo + " días"}</td>
                                <td>{equipo.enc_mant_predictivo}</td>


                                {/* {Array.from({ length: 15 }).map((_, index) => (
                                <td key={index}>Table cell {index}</td>
                                ))} */}
                                {/* <td>{idx}</td>
                                <td key={idx}>Table cell {idx}</td>
                                <td key={idx}>Table cell {idx}</td> */}
                            </tr>
                        )
                    })
                }        
                
                {/* <tr>
                <td>1</td>
                {Array.from({ length: 15 }).map((_, index) => (
                    <td key={index}>Table cell {index}</td>
                ))}
                </tr> */}



                {/* <tr>
                <td>1</td>
                {Array.from({ length: 15 }).map((_, index) => (
                    <td key={index}>Table cell {index}</td>
                ))}
                </tr>
                <tr>
                <td>2</td>
                {Array.from({ length: 15 }).map((_, index) => (
                    <td key={index}>Table cell {index}</td>
                ))}
                </tr>
                <tr>
                <td>3</td>
                {Array.from({ length: 15 }).map((_, index) => (
                    <td key={index}>Table cell {index}</td>
                ))}
                </tr> */}
            </tbody>
            </Table>

            {/* <Table responsive>
            <thead>
                <tr>
                <th>#</th>
                {Array.from({ length: 15 }).map((_, index) => (
                    <th key={index}>Table heading</th>
                ))}
                </tr>
            </thead>
            <tbody>
                <tr>
                <td>1</td>
                {Array.from({ length: 15 }).map((_, index) => (
                    <td key={index}>Table cell {index}</td>
                ))}
                </tr>
                <tr>
                <td>2</td>
                {Array.from({ length: 15 }).map((_, index) => (
                    <td key={index}>Table cell {index}</td>
                ))}
                </tr>
                <tr>
                <td>3</td>
                {Array.from({ length: 15 }).map((_, index) => (
                    <td key={index}>Table cell {index}</td>
                ))}
                </tr>
            </tbody>
            </Table> */}

            </div>
        </div>
    )
}

/* ListaTodosEquipos.getInitialProps = async (ctx) => {
    const equiposResponse = await fetch('http://localhost:3000/api/todos-los-equipos/');
    const equipos = await equiposResponse.json();
    console.log(equipos)
    return {
      props: equipos
    }
}
 */
export default ListaTodosEquipos
