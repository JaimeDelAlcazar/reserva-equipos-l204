import React from 'react'
// import NavbarEquipos from '../components/Navbar/Navbar'
import PanelNav from '../../components/PanelNav/PanelNav'


const Layout: React.FC = ({ children }) => {
    return (
        <div className="contendor_pag">
            <PanelNav />

            {/* <NavbarEquipos /> */}
            {/* {children} */}
            <div className="seccion_principal" >
            {/* <div > */}
            {children}
            </div>
            
            {/* <footer>Footer</footer> */}
        </div>
    )
}

export default Layout
