import React, {useRef, useState} from 'react'
import { Form, Container, Col, Row, Button, Table } from 'react-bootstrap'
import Link from 'next/link'

import { withRouter } from 'next/router';
import config from '../../config.js';


// import PanelSeleccion from '../PanelSeleccion/PanelSeleccion';




const PanelSolicitud = ({json, shown}) => {
// const PanelSolicitud = ({json},{shown}) => {
// const PanelSolicitud = ({props}) => {
// const PanelSolicitud = ({props}, { router: { query } }) => {
    // const object = JSON.parse(query.object);

    const data = json;

    // const [data, setData] = useState('')
    // setData(json);
    // console.log('query object',object)
    // console.log('PROPS',props)
    console.log('shown',shown)
    // let asd = data.json()
    // JSON.parse(data)
    console.log('json',data)
    // console.log('json.nombre_proyecto', json.nombre_proyecto)
    // console.log('json.nombre_proyecto', json[nombre_proyecto])
    const table_header = ['Nombre','Marca','Fecha próxima de salida a campo','Fecha próxima de devolución a UTEC','Fecha fin de mantenimiento correctivo','Solicitar'];
    // const table_header = ['Nombre','Marca','Estado','Fecha de salida','Fecha de devolución','Solicitar'];



    async function postData() {
        // setData(json);
        // let data = json;
        console.log('postData json',data)
        // const response = await fetch('http://localhost:3000/api/solicitud/', {
        const response = await fetch(`${config.domain}/api/solicitud/`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            nombre_proyecto: 'test1',
            // nombre_proyecto: data.nombre_proyecto,
            ubicacion: data.ubicacion,
            nombre_solicitante: data.nombre_solicitante,
            correo_solicitante: data.correo_solicitante,
            coord_campo: data.coord_campo,
            correo_coord_campo: data.correo_coord_campo,
            coord_tecnico: data.coord_tecnico,
            correo_coord_tecnico: data.correo_coord_tecnico,
            coord_logistico: data.coord_logistico,
            correo_coord_logistico: data.correo_coord_logistico,
            fecha_salida: data.fecha_salida,
            fechar_retorno: data.fechar_retorno,
          })
        //   body: JSON.stringify({
        //     nombre_proyecto: nombre_proyecto.current?.value,
        //     ubicacion: ubicacion.current?.value,
        //     nombre_solicitante: nombre_solicitante.current?.value,
        //     correo_solicitante: correo_solicitante.current?.value,
        //     coord_campo: coord_campo.current?.value,
        //     correo_coord_campo: correo_coord_campo.current?.value,
        //     coord_tecnico: coord_tecnico.current?.value,
        //     correo_coord_tecnico: correo_coord_tecnico.current?.value,
        //     coord_logistico: coord_logistico.current?.value,
        //     correo_coord_logistico: correo_coord_logistico.current?.value,
        //     fecha_salida: fecha_salida.current?.value,
        //     fechar_retorno: fechar_retorno.current?.value,
        //   })
        });
        return response.json(); 
    }






    return (
        <>
        { 
            // console.log(shown)
            shown == true &&
            <div className="contenedor_seleccion">
                <Form className="form_seleccion">
                <Form.Row className="form_row_search">
                    <Form.Group className="search_container">
                    <Form.Control type="text" placeholder="Buscar equipos ..."    className="input_search"/>
                    </Form.Group>
                    

                    <span className="checkbox_label">Mostrar seleccionados <input id="display_selected" type="checkbox"/></span>
                    {/* <Form.Group id="formGridCheckbox">
                        <Form.Check type="checkbox" label="Mostrar seleccionados" />
                        
                        <span><Form.Check type="checkbox" /></span>
                        
                    </Form.Group> */}
                    
                </Form.Row>

                
                {/* <Table responsive >
                <thead>
                    <tr>
                    <th></th>
                    {Array.from({ length: table_header.length }).map((_, index) => (
                        <th key={index} className="th_table_equipos">{table_header[index]}</th>
                    ))}
                    </tr>
                </thead>
                <tbody>

                    {
                        props.map(function(equipo, idx){
                            return (
                                <tr key={idx}>
                                    <td>{idx + 1}</td>
                                    <td>{equipo.nombre}</td>
                                    <td>{equipo.marca}</td>
                                    <td>{equipo.fecha_salida}</td>
                                    <td>{equipo.fecha_devolucion}</td>
                                    <td> </td>
                                    <td className="td_table_center"><input type="checkbox" onclick="" />

                                    </td>
                                </tr>
                            )
                        })
                    }        




            
                </tbody>
                </Table> */}



                {/* <Form.Group id="formGridCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group> */}

                {/* <Button variant="info" type="submit" size="lg"  className="btn_solicitar_equipos">
                    Confirmar solicitud
                </Button> */}

                <Link href="/"  as={`/`} passHref>
                    <Button variant="info" type="submit" size="lg"  className="btn_confirmar_solicitud" onClick={postData}>
                        Confirmar solicitud
                    </Button>
                </Link>
                </Form>





            {/*  */}



            {/* <PanelSeleccion />    */}



            </div>
        }
        </>
        




        
    )
}

// PanelSolicitud.getInitialProps = async (ctx) => {
PanelSolicitud.getInitialProps = async () => {
    const equiposResponse = await fetch(`${config.domain}/api/todos-los-equipos/`);
    const equipos = await equiposResponse.json();
    console.log(equipos)
    return {
      // posts
      // props: {posts}
    //   shown: {shown},
      props: equipos
    }
  }

export default PanelSolicitud
