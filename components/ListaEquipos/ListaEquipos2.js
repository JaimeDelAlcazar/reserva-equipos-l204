import React,{ Component, useState, useEffect } from 'react'

import { Form, Container, Col, Row, Button } from 'react-bootstrap'
import Link from 'next/link'
import { render } from 'react-dom';

const ListaEquipos = ({props}) => {

    // console.log(props)

    return (
        <div className="contenedor_seleccion">

            <Form className="form_lista_equipos">


            <Container>
            <Row>
                <Col xs={3} className="col_header">Nombre</Col>
                <Col xs={2} className="col_header">Estado</Col>
                <Col xs={2} className="col_header">Fecha de salida</Col>
                <Col xs={3} className="col_header">Fecha de devolución</Col>
                <Col xs={2} className="col_header">Ubicación</Col>
            </Row>
            </Container>

            {/*  */}

            <Container>
            {/* <Row>
                <Col xs={3} className="col_lista">Drone Phantom 4 Pro</Col>
                <Col xs={2} className="col_lista">Disponible</Col>
                <Col xs={2} className="col_lista">26 feb</Col>
                <Col xs={3} className="col_lista">31 mar</Col>
                <Col xs={2} className="col_lista">L204</Col>
            </Row> */}
            <Row>
                {/* <Col xs={3} className="col_lista">{props[3].nombre}</Col> */}
                <Col xs={3} className="col_lista">Drone Phantom 4 Pro</Col>
                <Col xs={2} className="col_lista">Disponible</Col>
                <Col xs={2} className="col_lista">26 feb</Col>
                <Col xs={3} className="col_lista">31 mar</Col>
                <Col xs={2} className="col_lista">L204</Col>
            </Row>
            <Row>
                <Col xs={3} className="col_lista">Drone Phantom 4 Pro</Col>
                <Col xs={2} className="col_lista">Disponible</Col>
                <Col xs={2} className="col_lista">26 feb</Col>
                <Col xs={3} className="col_lista">31 mar</Col>
                <Col xs={2} className="col_lista">L204</Col>
            </Row>
            <Row>
                <Col xs={3} className="col_lista">Drone Phantom 4 Pro</Col>
                <Col xs={2} className="col_lista">Disponible</Col>
                <Col xs={2} className="col_lista">26 feb</Col>
                <Col xs={3} className="col_lista">31 mar</Col>
                <Col xs={2} className="col_lista">L204</Col>
            </Row>
            <Row>
                <Col xs={3} className="col_lista">Drone Phantom 4 Pro</Col>
                <Col xs={2} className="col_lista">Disponible</Col>
                <Col xs={2} className="col_lista">26 feb</Col>
                <Col xs={3} className="col_lista">31 mar</Col>
                <Col xs={2} className="col_lista">L204</Col>
            </Row>
            

            </Container>

            <Link href="/"  as={`/`} passHref>
                <Button variant="info" type="submit" size="lg"  className="btn_confirmar_solicitud">
                    Confirmar solicitud
                </Button>
            </Link>
            </Form>
        </div>
    )
}


ListaEquipos.getInitialProps = async (ctx) => {
    const equiposResponse = await fetch('http://localhost:3000/api/todos-los-equipos/');
    const equipos = await equiposResponse.json();
    console.log(equipos)
    return {
      // posts
      // props: {posts}
      props: equipos
    }
  }

export default ListaEquipos
