import React from 'react'
import { Button } from 'react-bootstrap'
import Link from 'next/link'


const PanelNav = () => {
    return (
        // <div>
            <div className="contendor_nav">
                {/* <img>LOGO CITA</img> */}
                <img className="logo_nav" src="/images/Logo_CITA.png"/>
                {/* <div>Nueva Solcitud</div> */}
                {/* <Button variant="light" size="lg" className="btn_nueva_solicitud">Nueva Solicitud</Button> */}
                {/* <div className="nav_link">Equipos</div> */}
                {/* <div className="nav_link">Contacto</div> */}
            
                <Link href="/solicitud/"  as={`/solicitud/`} passHref>
                    <Button variant="light" size="lg" className="btn_nueva_solicitud">Nueva Solicitud</Button>
                </Link>
                <Link href="/todos-los-equipos/"  as={`/todos-los-equipos/`} passHref>
                    <div className="nav_link">Equipos</div>
                </Link>
                {/* <Link href="/contacto/"  as={`/contacto/`} passHref>
                    <div className="nav_link">Contacto</div>
                </Link> */}
                {/* <div className="nav_link">Procedimientos</div> */}
                <Link href="/protocolo/"  as={`/protocolo/`} passHref>
                    <div className="nav_link">Protocolo</div>
                </Link>
            </div>
        // </div>
    )
}

export default PanelNav
