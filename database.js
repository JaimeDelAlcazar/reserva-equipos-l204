const mysql = require('mysql');
const util = require( 'util' );
const config = require('./config.js');
const moment = require("moment");

module.exports = class DatabaseConnection {

	constructor() {
		this.handleCon();
	}

	handleCon() {
		/* this.connection = mysql.createConnection({
			host     : config.mysql.host,
			user     : config.mysql.user,
			password : config.mysql.password,
			database : config.mysql.database,
		}); */

		this.connection = mysql.createConnection({
			host     : config.mysql.host,
			user     : config.mysql.user,
			password : config.mysql.password,
			database : config.mysql.database,
			// port     : config.mysql.port,
		});

		this.connection.connect((err) => {
			if (err) {
			console.error('[db err]', err);
			setTimeout(this.handleCon, 2000);
			} else {
			console.log('DB Connected!');
			}
		});

		this.connection.on('error', err => {
			console.error('[db err]', err);
			if (err.code === 'PROTOCOL_CONNECTION_LOST') {
				this.handleCon();
			} else {
				console.error('[db err]', err);
			}
		})
	}


	startConnection() {
		return util.promisify(this.connection.connect).call(this.connection);
	}


	finishConnection() {
		return util.promisify(this.connection.end).call(this.connection);
	}

	async query(sql_query) {
		await util.promisify(this.connection.query).call(this.connection, sql_query);
	}

	async queryGet(sql_query) {
		let result = await util.promisify(this.connection.query).call(this.connection, sql_query);
		return result;
	}

	
	async getEquipos () {
		// SELECT * FROM 'Equipos_L204'
		let jsons = await this.queryGet('SELECT * FROM Equipos');
		// let jsons = await this.queryGet('SELECT * FROM Equipos WHERE id < 63');
		return jsons;
	}

	async getHistorial () {
		// SELECT * FROM 'Equipos_L204'
		// let jsons = await this.queryGet('SELECT * FROM Historial WHERE id_solicitud < 7');
		let jsons = await this.queryGet('SELECT * FROM Historial');
		return jsons;
	}



	async addSolicitud(nombre_proyecto, ubicacion, nombre_solicitante, correo_solicitante, coord_campo, correo_coord_campo, coord_tecnico, correo_coord_tecnico, 	coord_logistico, correo_coord_logistico, estado_solicitud, equipo_id, nombre_equipo, modelo_eq, marca_eq, nro_serie_eq, nro_inv_eq, especialista, fecha_salida_progr,fecha_devolucion_progr,fecha_ini_mant_predictvo,fecha_fin_mant_predictivo,fecha_fin_mant_correctivo,  fecha_registro) {
		let insert_sql;
		// console.log('fecha_ini_mant_predictvo',fecha_ini_mant_predictvo)
		if ((fecha_ini_mant_predictvo != 'Invalid date')&&(fecha_fin_mant_predictivo != 'Invalid date')&&(fecha_fin_mant_correctivo != 'Invalid date')) {
			// fecha_ini_mant_predictvo = moment().add(1,'years').format('DD/MM/YYYY ')

			insert_sql = `INSERT INTO Historial(nombre_proyecto, ubicacion, nombre_solicitante, correo_solicitante, coord_campo, correo_coord_campo, coord_tecnico, correo_coord_tecnico, coord_logistico, correo_coord_logistico, estado_solicitud, equipo_id, nombre_equipo, modelo_eq, marca_eq, nro_serie_eq, nro_inv_eq, especialista, fecha_salida_progr,fecha_devolucion_progr,fecha_ini_mant_predictvo,fecha_fin_mant_predictivo,fecha_fin_mant_correctivo,  fecha_registro ) VALUES('${nombre_proyecto}', '${ubicacion}', '${nombre_solicitante}', '${correo_solicitante}', '${coord_campo}', '${correo_coord_campo}', '${coord_tecnico}', '${correo_coord_tecnico}', '${coord_logistico}', '${correo_coord_logistico}', '${estado_solicitud}', '${equipo_id}', '${nombre_equipo}', '${modelo_eq}', '${marca_eq}', '${nro_serie_eq}', '${nro_inv_eq}', '${especialista}', STR_TO_DATE('${fecha_salida_progr}','%d/%m/%Y'), STR_TO_DATE('${fecha_devolucion_progr}','%d/%m/%Y'), STR_TO_DATE('${fecha_ini_mant_predictvo}','%d/%m/%Y'), STR_TO_DATE('${fecha_fin_mant_predictivo}','%d/%m/%Y'), STR_TO_DATE('${fecha_fin_mant_correctivo}','%d/%m/%Y'),    STR_TO_DATE('${fecha_registro}','%d/%m/%Y'))`;

		}else {
			insert_sql = `INSERT INTO Historial(nombre_proyecto, ubicacion, nombre_solicitante, correo_solicitante, coord_campo, correo_coord_campo, coord_tecnico, correo_coord_tecnico, coord_logistico, correo_coord_logistico, estado_solicitud, equipo_id, nombre_equipo, modelo_eq, marca_eq, nro_serie_eq, nro_inv_eq, especialista, fecha_salida_progr,fecha_devolucion_progr,   fecha_registro ) VALUES('${nombre_proyecto}', '${ubicacion}', '${nombre_solicitante}', '${correo_solicitante}', '${coord_campo}', '${correo_coord_campo}', '${coord_tecnico}', '${correo_coord_tecnico}', '${coord_logistico}', '${correo_coord_logistico}', '${estado_solicitud}', '${equipo_id}', '${nombre_equipo}', '${modelo_eq}', '${marca_eq}', '${nro_serie_eq}', '${nro_inv_eq}', '${especialista}', STR_TO_DATE('${fecha_salida_progr}','%d/%m/%Y'), STR_TO_DATE('${fecha_devolucion_progr}','%d/%m/%Y'),    STR_TO_DATE('${fecha_registro}','%d/%m/%Y'))`;
		}
		await this.query(insert_sql);
	}


	async updateEstadoById(id_solicitud,estado_solicitud,comentarios) {

		let insert_sql;
		// insert_sql = `UPDATE Historial SET estado_solicitud='${estado_solicitud}' WHERE id_solicitud='${id_solicitud}'`;
		insert_sql = `UPDATE Historial SET estado_solicitud='${estado_solicitud}', comentarios='${comentarios}' WHERE id_solicitud='${id_solicitud}'`;
		await this.query(insert_sql);
	}

	async updateDates(equipo_id) {
		let select_sql;
		let select_sql_fecha_salida_progr;

		select_sql_fecha_salida_progr = `SELECT fecha_salida_progr FROM Historial WHERE estado_solicitud='Aprobado' AND equipo_id='${equipo_id}' AND fecha_salida_progr IS NOT NULL ORDER BY fecha_salida_progr ASC`;
		let json_fecha_salida_progr= await this.queryGet(select_sql_fecha_salida_progr);
		
		
		if (json_fecha_salida_progr[0] == undefined) return;
		console.log('equipo_id',equipo_id);
		console.log('json_fecha_salida_progr',json_fecha_salida_progr[0].fecha_salida_progr);
		// 


		let insert_sql_fecha_salida_progr;

		insert_sql_fecha_salida_progr = `UPDATE Equipos SET fecha_salida =STR_TO_DATE('${moment(json_fecha_salida_progr[0].fecha_salida_progr).format('DD/MM/YYYY ')}','%d/%m/%Y') WHERE id='${equipo_id}'`;

		await this.query(insert_sql_fecha_salida_progr);

		// return json_fecha_salida_progr;


		//

		let select_sql_fecha_devolucion_progr;
		select_sql_fecha_devolucion_progr = `SELECT fecha_devolucion_progr FROM Historial WHERE estado_solicitud='Aprobado' AND equipo_id='${equipo_id}' AND fecha_devolucion_progr IS NOT NULL ORDER BY fecha_devolucion_progr ASC`;
		let json_fecha_devolucion_progr= await this.queryGet(select_sql_fecha_devolucion_progr);
		
		if (json_fecha_devolucion_progr[0] == undefined) return;
		console.log('equipo_id',equipo_id);
		console.log('json_fecha_devolucion_progr',json_fecha_devolucion_progr[0].fecha_devolucion_progr);
		let insert_sql_fecha_devolucion_progr;
		insert_sql_fecha_devolucion_progr = `UPDATE Equipos SET fecha_devolucion =STR_TO_DATE('${moment(json_fecha_devolucion_progr[0].fecha_devolucion_progr).format('DD/MM/YYYY ')}','%d/%m/%Y') WHERE id='${equipo_id}'`;
		await this.query(insert_sql_fecha_devolucion_progr);
	}
		
}