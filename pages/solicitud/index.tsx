import React from 'react'
import Link from 'next/link'
import { unmountComponentAtNode, render } from "react-dom";

import PanelNav from '../../components/PanelNav/PanelNav';
import PanelSolicitud from '../../components/PanelSolicitud/PanelSolicitud';
import PanelSeleccion from '../../components/PanelSeleccion/PanelSeleccion';
import config from '../../config.js';

const NuevaSolicitud = ({props}) => {

    // function handleClick() {
        // alert('Hola mundo');
        // unmountComponentAtNode(document.getElementById('123'));

        /* var x = document.getElementById("123");
        if (x.style.display === "none") {
        x.style.display = "block";
        } else {
        x.style.display = "none";
        } */


        // findDOMNode(this.refs.myinput).focus();
    //   }

    return (
        // <div className="contendor_pag">
        <>
            {/* <PanelNav /> */}
            {/* <div className="seccion_principal"  onClick={handleClick} > */}
                <PanelSolicitud props={props}/>
                
                {/* <PanelSeleccion /> */}
                {/* <div id="123">ASDASD</div> */}
            {/* </div> */}
            {/* <Link href="/equipos/"></Link> */}
            {/* <Link href="/equipos/"  as={`/equipos/`} passHref>1423423423424</Link> */}
            {/* <a href="/equipos/">123</a> */}
        </>
    )
}

NuevaSolicitud.getInitialProps = async (ctx) => {
    // const equiposResponse = await fetch('http://localhost:3000/api/todos-los-equipos/');
    // const equiposResponse = await fetch('/api/todos-los-equipos/');
    // const equiposResponse = await fetch('https://reserva-equipos-l204.vercel.app/api/todos-los-equipos/');
    const equiposResponse = await fetch(`${config.domain}/api/todos-los-equipos/`);
    const equipos = await equiposResponse.json();
    console.log('equipos',equipos)
    return {
      props: equipos
    }
  }


export default NuevaSolicitud
