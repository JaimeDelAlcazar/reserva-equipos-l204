import React, {useRef, useState} from 'react'
import { Form, Container, Col, Row, Button, Table, Alert} from 'react-bootstrap'
import Link from 'next/link'
import moment from 'moment'
import { constants } from 'buffer'
import config from '../../config.js';
import Email from '../../smtp.js';

const Admin = ({props}) => {


    const table_header = ['id_solicitud','nombre_solicitante','correo_solicitante','nombre_proyecto','equipo_id','nombre_equipo','modelo_eq','marca_eq','nro_serie_eq','nro_inv_eq','especialista','ubicacion','coord_campo','correo_coord_campo','coord_tecnico','correo_coord_tecnico','coord_logistico','correo_coord_logistico','fecha_salida_progr','fecha_devolucion_progr','fecha_ini_mant_predictivo','fecha_fin_mant_predictivo','fecha_fin_mant_correctivo','fecha_registro','comentarios','estado_solicitud'];


    const [changed, setChanged] = useState([]);
    // const buscador  =   React.useRef(null);
    const buscador  =   useRef(null);


    const filtrar = () => {
        // console.log(buscador.current?.value)
        const texto = buscador.current?.value.toLowerCase();



        for (let i = 0; i < props.length; i++) {


            if (((props[i].nombre_solicitante).toLowerCase().indexOf(texto) !== -1) || ((props[i].correo_solicitante).toLowerCase().indexOf(texto) !== -1) || ((props[i].nombre_proyecto).toLowerCase().indexOf(texto) !== -1) || ((props[i].nombre_equipo).toLowerCase().indexOf(texto) !== -1)  ) {

                (document.getElementsByClassName('row_equipo') as HTMLCollectionOf<HTMLElement>)[i].style.display = "table-row";
                console.log('props[i]',props[i])
            } else {

                document.querySelectorAll<HTMLElement>('.row_equipo')[i].style.display = "none";
            }



        }

    }
    

    function handleChange() {
        // console.log("Fruit Selected!!");
        // this.setState({ fruit: e.target.value });
      }

      async function findSolicitudById(id,allSelect) {
          var a;
        for (let i = 0; i < props.length; i++) {
            if (props[i].id_solicitud ==  id) {
                console.log('id prop changed', id);
                a = {
                    // id_solicitud: changed,
                    // estado_solicitud: selectEstado.value,
                    // estado_solicitud:  allSelect[i].value,
                    estado_solicitud: await allSelect[i].value,
                    equipo_id: await props[i].equipo_id
                }
                // return a

            } else {
            //     console.log('NO');
            //   return
            }
        }

      return [a.estado_solicitud,a.equipo_id];
    }

    
      async function updateData() {


        document.querySelectorAll<HTMLButtonElement>('.btn_actualizar_db')[0].disabled = true;
        
        console.log('changed = ',changed);
        const allSelect = document.querySelectorAll('select');
        // const selectEstado = allSelect[i];


        const allTextArea = document.querySelectorAll('textarea');


        var data = [];
        var json_solicitud ={


        };
        // var b = findSolicitudById(changed[0],allSelect);
        // console.log('b', b)

        // for (let i = 0; i < allSelect.length; i++) {

        // 

        let estado = "Pendiente";
        let id_ = 1;

        let comentarios_ = '';
        let comentarios1 = '';

        let email_ = '';

        let solicitante = []
        let solicitante_array = []

        let eq_name,eq_mail,eq_date = '';
        let eq_array = [];
        let mail_array = [];
        let date_array = [];

        let eq_coord_campo = [];
        let eq_coord_tecnico = [];
        let eq_coord_logistico = [];

        let mail_coord_campo = [];
        let mail_coord_tecnico = [];
        let mail_coord_logistico = [];
        for (let i = 0; i < changed.length; i++) {

            console.log('props.length',props.length)
            // console.log('props.length',props.length)

            for (let j = 0; j < props.length; j++) {
                // console.log('props[i]',props[j].id_solicitud)
                    // console.log('props. changed[i]',changed[i])
                if (props[j].id_solicitud ==  changed[i]) {
                    
                    // console.log('id prop changed', id);
                        estado = await allSelect[j].value;
                        id_= await props[j].equipo_id;

                        // comentarios_= await props[j].comentarios;
                        // comentarios1= await props[j].comentarios;
                        // comentarios_ = comentarios1 == null? 'a' : comentarios1;
                        comentarios_ = await allTextArea[j].value;
                        // estado = "Rechazado";
                        // id_= 3;
                        email_ = await props[j].correo_solicitante;


                        eq_name =  await props[j].nombre_equipo;
                        eq_mail =  await props[j].correo_solicitante;
                        eq_date =  await props[j].fecha_salida_progr;

                        eq_array.push(eq_name);
                        mail_array.push(eq_mail);
                        date_array.push(eq_date);

                        eq_coord_campo =  await props[j].correo_coord_campo;
                        eq_coord_tecnico =  await props[j].correo_coord_tecnico;
                        eq_coord_logistico =  await props[j].coord_logistico;

                        mail_coord_campo.push(eq_coord_campo);
                        mail_coord_tecnico.push(eq_coord_tecnico);
                        mail_coord_logistico.push(eq_coord_logistico);

                        solicitante =  await props[j].nombre_solicitante;
                        solicitante_array.push(solicitante);
                } 
                
            }

            json_solicitud =  await {
                id_solicitud: changed[i],
                estado_solicitud: estado,
                equipo_id: id_,
                comentarios: comentarios_,
            }

            data.push(json_solicitud);



        }
        console.log('data',data);


        // const selectEstado = allSelect[i];
        const response = await fetch(`${config.domain}/api/admin/`, {
            method: 'PUT',  
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            
        // 


// mail_array.push(eq_mail)  date_array.push(eq_date);

        // let total_eq_user = [];
        let total_eq_user_aprobados = [];
        let total_eq_user_rechazados = [];

        let eq_ap_str = '<ul>'
        let eq_rech_str = '<ul>'

        let sent_mail = [];

        for (let i = 0; i < mail_array.length; i++) {
            eq_ap_str = '<ul>'
            eq_rech_str = '<ul>'
            total_eq_user_aprobados = [];
            total_eq_user_rechazados = [];

            // let sent_mail = [];


            // estado = await allSelect[j].value;

            for (let j = 0; j < props.length; j++) {
                eq_ap_str = '<ul>'
                eq_rech_str = '<ul>'

                // console.log('props[i]',props[j].id_solicitud)
                    // console.log('props. changed[i]',changed[i])
                if (props[j].correo_solicitante ==  mail_array[i]) {


                    if (props[j].fecha_salida_progr == date_array[i]) {


                        // if (props[j].estado_solicitud == 'Aprobado') {
                        if (allSelect[j].value == 'Aprobado') {
                            // let arr_aprobados = [props[j].nombre_equipo,props[j].modelo_eq,props[j].marca_eq,props[j].nro_serie_eq,props[j].nro_inv_eq,props[j].especialista]
                            let nombre_equipo = props[j].nombre_equipo;
                            let modelo_eq = props[j].modelo_eq && props[j].modelo_eq !== '---'? props[j].modelo_eq:'';
                            let marca_eq = props[j].marca_eq && props[j].marca_eq !== '---'? props[j].marca_eq:'';
                            let nro_serie_eq = props[j].nro_serie_eq && props[j].nro_serie_eq !== '---'? props[j].nro_serie_eq:'';
                            let nro_inv_eq = props[j].nro_inv_eq && props[j].nro_inv_eq !== '---'? props[j].nro_inv_eq:'';
                            let especialista = props[j].especialista && props[j].especialista !== '---'? props[j].especialista:'';

                            /* let fecha_salida_progr = props[j].fecha_salida_progr? props[j].fecha_salida_progr:'';
                            let fecha_devolucion_progr = props[j].fecha_devolucion_progr? props[j].fecha_devolucion_progr:''; */
                            let fecha_salida_progr = props[j].fecha_salida_progr? moment(props[j].fecha_salida_progr).format('DD/MM/YYYY'):'';
                            let fecha_devolucion_progr = props[j].fecha_devolucion_progr? moment(props[j].fecha_devolucion_progr).format('DD/MM/YYYY'):'';


                            let arr_aprobados = [nombre_equipo,modelo_eq,marca_eq,nro_serie_eq,nro_inv_eq,especialista,fecha_salida_progr,fecha_devolucion_progr]
                            // total_eq_user_aprobados.push(props[j].nombre_equipo)
                            total_eq_user_aprobados.push(arr_aprobados)

                            // console.log('props[j].estado_solicitud',props[j].estado_solicitud)
                            console.log('total_eq_user_aprobados ---',total_eq_user_aprobados)
                        }

                        // if (props[j].estado_solicitud == 'Rechazado') {
                        if (allSelect[j].value == 'Rechazado') {
                            let nombre_equipo = props[j].nombre_equipo;
                            let modelo_eq = props[j].modelo_eq && props[j].modelo_eq !== '---'? props[j].modelo_eq:'';
                            let marca_eq = props[j].marca_eq && props[j].marca_eq !== '---'? props[j].marca_eq:'';
                            let nro_serie_eq = props[j].nro_serie_eq && props[j].nro_serie_eq !== '---'? props[j].nro_serie_eq:'';
                            let nro_inv_eq = props[j].nro_inv_eq && props[j].nro_inv_eq !== '---'? props[j].nro_inv_eq:'';
                            let especialista = props[j].especialista && props[j].especialista !== '---'? props[j].especialista:'';
                            

                            /* let fecha_salida_progr = props[j].fecha_salida_progr? props[j].fecha_salida_progr:'';
                            let fecha_devolucion_progr = props[j].fecha_devolucion_progr? props[j].fecha_devolucion_progr:''; */
                            let fecha_salida_progr = props[j].fecha_salida_progr? moment(props[j].fecha_salida_progr).format('DD/MM/YYYY'):'';
                            let fecha_devolucion_progr = props[j].fecha_devolucion_progr? moment(props[j].fecha_devolucion_progr).format('DD/MM/YYYY'):'';

                            let arr_rechazados = [nombre_equipo,modelo_eq,marca_eq,nro_serie_eq,nro_inv_eq,especialista,fecha_salida_progr,fecha_devolucion_progr]

                            total_eq_user_rechazados.push(arr_rechazados)

                            console.log('total_eq_user_rechazados ---',total_eq_user_rechazados)
                            // total_eq_user_rechazados.push(props[j].nombre_equipo)

                            // console.log('props[j].estado_solicitud',props[j].estado_solicitud)
                        }
                        
                        console.log('total_eq_user_aprobados',total_eq_user_aprobados)
                        console.log('total_eq_user_rechazados',total_eq_user_rechazados)
                    }

                    


                } 
                
            }


            // let eq_ap_str = '<ul>'
            for (let i = 0; i < total_eq_user_aprobados.length; i++) {
                // eq_ap_str = eq_ap_str + '<li>' + total_eq_user_aprobados[i] + '</li>'
                eq_ap_str = eq_ap_str + '<li>' + total_eq_user_aprobados[i][0]+ '   '+total_eq_user_aprobados[i][1]+ '   '+total_eq_user_aprobados[i][2]+ '   '+total_eq_user_aprobados[i][3]+ '   '+total_eq_user_aprobados[i][4]+ '   '+total_eq_user_aprobados[i][5]        + '   '+total_eq_user_aprobados[i][6]+ ' - '+total_eq_user_aprobados[i][7]+ '</li>'
            }
            eq_ap_str = eq_ap_str + '<ul>'

            var str_eq_aprob = '';
            if (total_eq_user_aprobados.length != 0) {
                str_eq_aprob = "Se aprobó los siguientes equipos," + eq_ap_str
            }

            // let eq_rech_str = '<ul>'
            for (let i = 0; i < total_eq_user_rechazados.length; i++) {
                // eq_rech_str = eq_rech_str + '<li>' + total_eq_user_rechazados[i] + '</li>'
                eq_rech_str = eq_rech_str + '<li>' + total_eq_user_rechazados[i][0]+ '   '+total_eq_user_rechazados[i][1]+ '   '+total_eq_user_rechazados[i][2]+ '   '+total_eq_user_rechazados[i][3]+ '   '+total_eq_user_rechazados[i][4]+ '   '+total_eq_user_rechazados[i][5]           + '   '+total_eq_user_rechazados[i][6]+ ' - '+total_eq_user_rechazados[i][7] + '</li>'
            }
            eq_rech_str = eq_rech_str + '<ul>'

            var str_eq_rech = '';
            if (total_eq_user_rechazados.length != 0) {
                str_eq_rech = "Se rechazó los siguientes equipos," + eq_rech_str 
            }


            if ((sent_mail.includes(mail_array[i]) == false) || (sent_mail.includes(date_array[i]) == false)) {

                var listaDest = [];

                // listaDest.push(mail_array[i],mail_coord_campo[i],mail_coord_tecnico[i],mail_coord_logistico[i]);
                listaDest.push(mail_array[i],mail_coord_campo[i],mail_coord_tecnico[i],mail_coord_logistico[i]);
                /* if(listaDest.indexOf(item) === -1) {
                    this.items.push(item);
                    console.log(this.items);
                } */
                // listaDest.push(mail_array[i]);

                listaDest.forEach(async function (item) {
                    await Email.send({
                        Host : "smtp.gmail.com",
                        Username : "lmarin@utec.edu.pe",
                        // Password : "marinlynnguadalupe@10A",
                        //Password : "@10Alynn",
                        Password : "marinlynn10A@",
                        // To : email_,
                        // To : mail_array[i],
                        To : item,
                        From : "lmarin@utec.edu.pe",
                        Subject : "Confirmación de Reserva",
                        Body : "Se ha confirmado la solicitud de reserva."  + '<br/>' + "Nombre del solicitante: " + solicitante_array[i] + '<br/>' + '<br/>' + '<div>' + str_eq_aprob + '</div>' + '<div>' + str_eq_rech + '</div>' + '<br/>' + "CITA, UTEC.",
            
                    }).then(
                    // console.log('enviado')
                    );
                })

            }
            eq_ap_str = '<ul>'
            eq_rech_str = '<ul>'
            sent_mail.push(mail_array[i],date_array[i])
            

        }

        // await updateDates();
        setChanged([]);

        document.querySelectorAll<HTMLButtonElement>('.btn_actualizar_db')[0].disabled = false;
        await showSuccess();
        
    }


    async function showSuccess() {
        (document.getElementsByClassName('successMessage') as HTMLCollectionOf<HTMLElement>)[0].style.display = "block";
        // document.getElementsByClassName('btn_actualizar_db')[0].disabled = true;
        document.querySelectorAll<HTMLButtonElement>('.btn_actualizar_db')[0].disabled = true;
        setTimeout(function() {
            // document.getElementsByClassName('successMessage')[0].style.display = "none";
            (document.getElementsByClassName('successMessage') as HTMLCollectionOf<HTMLElement>)[0].style.display = "none";
            // document.getElementsByClassName('btn_actualizar_db')[0].disabled = false;
            document.querySelectorAll<HTMLButtonElement>('.btn_actualizar_db')[0].disabled = false;
            
            // Redirect to home
            // window.location.href = "http://localhost:3000/";
          }, 3500);
    }

    async function updateDates() {
        

    }
    
    return (
        // <>
        //     Admin
        // </>

        <div className="contenedor_seleccion_admin">
<script src="https://smtpjs.com/v3/smtp.js"></script>
            
            <Alert variant='success' className="successMessage" >Se actualizó el historial de solicitudes en la base de datos.</Alert>
            <div className="form_lista_equipos_admin">

            <input type="text" placeholder="Buscar equipos ..." className="admin_search" defaultValue='' ref={buscador} onKeyUp={filtrar}/>

            <Table   responsive bordered className="table">

            <thead>
                <tr>
                {/* <th></th> */}
                {/* <th>#</th> */}
                {Array.from({ length: table_header.length -1 }).map((_, index) => (
                    // <th key={index} className="th_table_equipos">{table_header[index]}</th>
                    <th  key={index} className="th_table_equipos">{table_header[index]}</th>
                ))
                }
                <th className="th_table_equipos td_table_state td_state_header">{table_header[table_header.length -1]}</th>
                
                </tr>
            </thead>
            <tbody>

                {
                    props.map(function(solicitud, idx){
                        return (
                            <tr key={idx} className="row_equipo">
                                <td className="td_table_center">{solicitud.id_solicitud}</td>
                                <td>{solicitud.nombre_solicitante}</td>
                                <td>{solicitud.correo_solicitante}</td>
                                <td>{solicitud.nombre_proyecto}</td>
                                <td className="td_table_center">{solicitud.equipo_id}</td>
                                <td>{solicitud.nombre_equipo}</td>
                                <td>{solicitud.modelo_eq}</td>
                                <td>{solicitud.marca_eq}</td>
                                <td>{solicitud.nro_serie_eq}</td>
                                <td>{solicitud.nro_inv_eq}</td>
                                <td>{solicitud.especialista}</td>
                                <td>{solicitud.ubicacion}</td>
                                <td>{solicitud.coord_campo}</td>
                                <td>{solicitud.correo_coord_campo}</td>
                                <td>{solicitud.coord_tecnico}</td>
                                <td>{solicitud.correo_coord_tecnico}</td>
                                <td>{solicitud.coord_logistico}</td>
                                <td>{solicitud.correo_coord_logistico}</td>
                                <td>{solicitud.fecha_salida_progr}</td>
                                <td>{solicitud.fecha_devolucion_progr}</td>
                                <td>{solicitud.fecha_ini_mant_predictvo}</td>
                                <td>{solicitud.fecha_fin_mant_predictivo}</td>
                                <td>{solicitud.fecha_fin_mant_correctivo}</td>
                                <td>{solicitud.fecha_registro}</td>

                                <td><textarea defaultValue={solicitud.comentarios}></textarea></td>
                                {/* <td><textarea value={solicitud.comentarios == null? null : solicitud.comentarios}  onChange={e => (console.log('Comment changed', e.target.value))}>{solicitud.comentarios}</textarea></td> */}
                                {/* <td><textarea value={solicitud.comentarios == null? null : solicitud.comentarios} >{solicitud.comentarios}</textarea></td> */}

                                <td className="td_table_center td_table_state">{

                                    <select defaultValue={solicitud.estado_solicitud == null? null : solicitud.estado_solicitud} onChange={e => (setChanged([...changed, solicitud.id_solicitud]))} className="selectEstadoSolicitud">
                                        <option value="Pendiente">Pendiente</option>
                                        <option value="Aprobado" >Aprobado</option>
                                        <option value="Rechazado">Rechazado </option>
                                        <option value="Finalizado">Finalizado</option>
                                    </select>
                                }</td>  
                                
                            </tr>
                        )
                    })
                }        
                


            </tbody>
            </Table>

            <Button variant="info" size="lg"  className="btn_actualizar_db" onClick={updateData}> Actualizar</Button>
            </div>
        </div>
    )
}

Admin.getInitialProps = async (ctx) => {

    const solicitudes = await fetch(`${config.domain}/api/admin/`);
    const solicitudesJson = await solicitudes.json();
    // console.log(solicitudesJson)
    return {
      props: solicitudesJson
    }
}

export default Admin
