import React from 'react'
import ListaTodosEquipos from '../../components/ListaTodosEquipos/ListaTodosEquipos';
// import { Form, Container, Col, Row, Button } from 'react-bootstrap'
// import Link from 'next/link'
// const config = require('./config.js');
// const config = require('./config.js');
import config from '../../config.js';


const TodosEquipos = ({props}) => {
    return (
        <ListaTodosEquipos props={props}/>
    )
}

TodosEquipos.getInitialProps = async (ctx) => {
    // const equiposResponse = await fetch('http://localhost:3000/api/todos-los-equipos/');
    // const equiposResponse = await fetch('/api/todos-los-equipos/');
    // const equiposResponse = await fetch('https://reserva-equipos-l204.vercel.app/api/todos-los-equipos/');
    // const equiposResponse = await fetch(`${config.domain}/api/todos-los-equipos/`);


    const equiposResponse = await fetch(`${config.domain}/api/todos-los-equipos/`, 
        {
            method: "GET",
            headers: {
                'User-Agent': '*',
                'Accept': "application/json; charset=UTF-8",
            }

    });


    const equipos = await equiposResponse.json();
    console.log(equipos)
    return {
      props: equipos
    }
}

export default TodosEquipos
