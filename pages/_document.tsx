import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head />
        {/* <Head>
          <title>Capacidades CITA</title>
        </Head> */}
        <body className="body-class">
        {/* <NextScript>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="https://smtpjs.com/v3/smtp.js"></script> 
        </NextScript> */}

          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument