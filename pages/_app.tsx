import { AppProps } from 'next/app'
import Head from 'next/head'
import 'bootstrap/dist/css/bootstrap.min.css';
import Layout from '../components/Layout/Layout'
import '../global_styles.css'


// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import '../date_picker.css'

function MyApp({ Component, pageProps }: AppProps) {
    return(
        <>
        <Head>
          <title>Reserva de Equipos L204</title>

          {/* <link rel="stylesheet" type="text/css" href="jquery.datetimepicker.css"/> */}
        </Head>
        {/* <Component {...pageProps} /> */}
        <Layout>
            <Component {...pageProps} />
        </Layout>
        </>
    )
  }
  
  // Only uncomment this method if you have blocking data requirements for
  // every single page in your application. This disables the ability to
  // perform automatic static optimization, causing every page in your app to
  // be server-side rendered.
  //
  // MyApp.getInitialProps = async (appContext) => {
  //   // calls page's `getInitialProps` and fills `appProps.pageProps`
  //   const appProps = await App.getInitialProps(appContext);
  //
  //   return { ...appProps }
  // }
  
  export default MyApp