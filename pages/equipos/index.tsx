import React from 'react'
import Layout from '../../components/Layout/Layout'
import PanelSeleccion from '../../components/PanelSeleccion/PanelSeleccion';
import { Form, Container, Col, Row, Button } from 'react-bootstrap'
import Link from 'next/link'
import config from '../../config.js';

// const EquiposDisponibles = ({props}, { router: { query } }) => {
const EquiposDisponibles = ({props}) => {

    return (

        <>
        
        </>
        // <PanelSeleccion props={props}/>
        // <PanelSeleccion props={props}/>

    )

}

EquiposDisponibles.getInitialProps = async (ctx) => {
    // const equiposResponse = await fetch('http://localhost:3000/api/todos-los-equipos/');
    // const equiposResponse = await fetch('/api/todos-los-equipos/');
    // const equiposResponse = await fetch('https://reserva-equipos-l204.vercel.app/api/todos-los-equipos/');
    const equiposResponse = await fetch(`${config.domain}/api/todos-los-equipos/`);
    const equipos = await equiposResponse.json();
    // console.log(equipos)
    return {
      props: equipos
    }
}

export default EquiposDisponibles