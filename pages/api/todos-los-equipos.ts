import React from 'react'
import { IncomingMessage, ServerResponse } from 'http'
import { NextApiRequest, NextApiResponse } from 'next'


const database = require('../../database.js');
var db = new database();


const lista = async (req: NextApiRequest, res: NextApiResponse)  => {
    let equipos = await db.getEquipos();
    // res.status(200).json(outputs)
    res.json(equipos)
}

export default lista
