import React from 'react'
import { IncomingMessage, ServerResponse } from 'http'
import { NextApiRequest, NextApiResponse } from 'next'

const database = require('../../database.js');
var db = new database();


const solicitud = async (req: NextApiRequest, res: NextApiResponse)  => {

    if (req.method === 'POST') {
        console.log('req.body', req.body);
        
        for (let i = 0; i < req.body.length; i++) {
       
            await db.addSolicitud(req.body[i].nombre_proyecto, req.body[i].ubicacion, req.body[i].nombre_solicitante, req.body[i].correo_solicitante, req.body[i].coord_campo, req.body[i].correo_coord_campo, req.body[i].coord_tecnico, req.body[i].correo_coord_tecnico, 	req.body[i].coord_logistico, req.body[i].correo_coord_logistico, req.body[i].estado_solicitud, req.body[i].equipo_id, req.body[i].nombre_equipo, req.body[i].modelo_eq, req.body[i].marca_eq, req.body[i].nro_serie_eq, req.body[i].nro_inv_eq, req.body[i].especialista,       req.body[i].fecha_salida_progr, req.body[i].fecha_devolucion_progr, req.body[i].fecha_ini_mant_predictvo, req.body[i].fecha_fin_mant_predictivo, req.body[i].fecha_fin_mant_correctivo,   req.body[i].fecha_registro);            
        }

    }

    let equipos = await db.getEquipos();
    res.json(equipos)
}

export default solicitud