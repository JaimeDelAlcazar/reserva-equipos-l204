import React from 'react'
import Link from 'next/link'
import { unmountComponentAtNode, render } from "react-dom";

import PanelNav from '../components/PanelNav/PanelNav';
import PanelSolicitud from '../components/PanelSolicitud/PanelSolicitud';
import PanelSeleccion from '../components/PanelSeleccion/PanelSeleccion';

// const config = require('../config.js');

const home = () => {
    return (
        <div className="contenedor_home">
            <Link href="/todos-los-equipos/"  as={`/todos-los-equipos/`} passHref>
                <div className="hero_home">
                {/* <img className="hero_image" src="../public/images/lista.svg"/> */}
                {/* <img className="hero_image" src="/images/Logo_CITA.png"/> */}
                <img className="hero_image" src="/images/lista.svg"/>
                Lista de equipos
                </div>
                {/* <div className="hero_home">Lista de equipos</div> */}
                {/* <img className="logo_nav" src="../public/images/lista.svg"/> */}

            </Link> 
            <Link href="/solicitud/"  as={`/solicitud/`} passHref>
                <div className="hero_home">
                <img className="hero_image_2" src="/images/reserva.svg"/>
                    Solicitud de reserva</div>
            </Link>
            {/* <div className="hero_home">Lista de equipos</div> */}
            {/* <div className="hero_home">Solicitud de reserva</div> */}
        </div>
    )
}

export default home

